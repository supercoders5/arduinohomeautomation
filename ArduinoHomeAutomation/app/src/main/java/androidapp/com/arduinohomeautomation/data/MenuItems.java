package androidapp.com.arduinohomeautomation.data;

/**
 * Created by sandip.mahajan on 1/25/2015.
 */
public class MenuItems {

    public final static int KITCHEN_MENU = 1;
    public final static int FAMILY_ROOM_MENU = 2;
    public final static int BATHROOM_MENU = 3;
    public final static int STORE_ROOM_MENU = 4;
    public final static int LAUNDRY_ROOM_MENU = 5;
    public final static int OTHER_ROOM_MENU = 6;

    public final static String[] mainScreenMenuItems = {"Rooms", "Kitchen", "Family Room", "Bathroom", "Store Room", "Laundry Room", "Other"};

    public final static String[] settingsMenuItems = {"FTP Server Connection", "FTP Server File Settings"};

    private final static String[] kitchenStoreLaundryOtherMenu = {"Lights", "Temperature", "Humidity", "Motion", "Sound", "Camera", "Water", "Gas / Smoke"};

    private final static String[] familyRoomMenu = {"Lights", "Temperature", "Humidity", "Motion", "Sound", "Camera"};

    private final static String[] bathroomMenu = {"Lights", "Temperature", "Humidity", "Motion", "Water", "Gas / Smoke"};

    public static String[] getItems(int type) {
        switch (type) {
            case KITCHEN_MENU:
            case STORE_ROOM_MENU:
            case LAUNDRY_ROOM_MENU:
            case OTHER_ROOM_MENU:
                return kitchenStoreLaundryOtherMenu;
            case FAMILY_ROOM_MENU:
                return familyRoomMenu;
            case BATHROOM_MENU:
                return bathroomMenu;
        }
        return null;
    }

    public final static int LIGHT_PREFERENCE_MENU = 0;
    public final static int TEMPERATURE_PREFERENCE_MENU = 1;
    public final static int HUMIDITY_PREFERENCE_MENU = 2;
    public final static int MOTION_PREFERENCE_MENU = 3;
    public final static int SOUND_PREFERENCE_MENU = 4;
    public final static int CAMERA_PREFERENCE_MENU = 5;
    public final static int WATER_PREFERENCE_MENU = 6;
    public final static int GAS_SMOKE_PREFERENCE_MENU = 7;

    public final static String[] cameraVideoMenu = {"Record Video", "Saved Videos", "Saved Pictures"};

    public final static int RECORD_VIDEO_MENU = 0;
    public final static int SAVED_VIDEOS_MENU = 1;
    public final static int SAVED_PICTURES_MENU = 2;
    public final static int RECORD_SOUND_MENU = 3;
}
