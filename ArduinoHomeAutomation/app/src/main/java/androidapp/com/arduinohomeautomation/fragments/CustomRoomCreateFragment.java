package androidapp.com.arduinohomeautomation.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidapp.com.arduinohomeautomation.R;


public class CustomRoomCreateFragment extends Fragment {

    private EditText roomName;
    private EditText monitoringFileName;
    private EditText controllingFileName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_create_room, container, false);
        roomName = (EditText) rootView.findViewById(R.id.room_name_edit);
        monitoringFileName = (EditText) rootView.findViewById(R.id.monitoring_file_edit);
        controllingFileName = (EditText) rootView.findViewById(R.id.controlling_file_edit);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_save:
                // TODO : Handle room save action
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
