package androidapp.com.arduinohomeautomation;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

import androidapp.com.arduinohomeautomation.constants.UIConstant;
import androidapp.com.arduinohomeautomation.util.UIUtil;


public class SplashScreen extends Activity {

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Set UI to this screen
         */
        setContentView(R.layout.activity_splash_screen);

        /**
         * Handler to hold the screen for pre defined time
         */
        handler = new Handler();

        /**
         * Set post delayed with define time to show splash screen to the user
         */
        handler.postDelayed(splashScreenFinishTask, UIConstant.SPLASH_SCREEN_TIMEOUT);
    }

    /**
     * Runnable to stop splash screen and navigate to home screen
     */
    private Runnable splashScreenFinishTask = new Runnable() {
        public void run() {
            /**
             * Remove runnable object
             */
            handler.removeCallbacks(splashScreenFinishTask);
            /**
             * Navigate UI to Home screen
             */
            UIUtil.navigateUI(getApplicationContext(), MainUIScreen.class);

            /**
             * Remove current screen from Android system stack
             */
            finish();
        }
    };
}
