package androidapp.com.arduinohomeautomation.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;

import androidapp.com.arduinohomeautomation.MainUIScreen;
import androidapp.com.arduinohomeautomation.R;
import androidapp.com.arduinohomeautomation.data.MenuItems;

/**
 * Created by sandip.mahajan on 1/26/2015.
 */
public class SoundPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private String screenName;
    private ListPreference mListPreference;

    public static SoundPreferenceFragment instance(String screenName) {
        SoundPreferenceFragment lightPreferenceFragment = new SoundPreferenceFragment();
        lightPreferenceFragment.screenName = screenName;
        return lightPreferenceFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.sound_preference);
        // Set listener :
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        Preference preference = findPreference("sound_preference_title");
        preference.setTitle(screenName);
        Preference detect_sound = findPreference("detect_sound_preference");
        detect_sound.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                return false;
            }
        });
        Preference recording = findPreference("recordings_sound_preference");
        recording.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Fragment fragment = SavedPicturesVideosFragment.instance(screenName, MenuItems.RECORD_SOUND_MENU);
                switchUI(fragment);
                return false;
            }
        });

        mListPreference = (ListPreference) findPreference("sound_preference_record_sound_pref");
    }

    private void switchUI(Fragment fragment) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,
                fragment).addToBackStack(null).commit();
        ((MainUIScreen) getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainUIScreen) getActivity()).mDrawerToggle.setDrawerIndicatorEnabled(false);
        ((MainUIScreen) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if ("sound_preference_record_sound_pref".equalsIgnoreCase(key)) {
            mListPreference.setSummary(mListPreference.getEntry().toString());
        }
    }
}
