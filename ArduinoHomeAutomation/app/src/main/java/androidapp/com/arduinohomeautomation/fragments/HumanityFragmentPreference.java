package androidapp.com.arduinohomeautomation.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidapp.com.arduinohomeautomation.R;

/**
 * Created by sandip.mahajan on 1/26/2015.
 */
public class HumanityFragmentPreference extends PreferenceFragment {

    private String screenName;

    public static HumanityFragmentPreference instance(String screenName) {
        HumanityFragmentPreference temperatureFragmentPreference = new HumanityFragmentPreference();
        temperatureFragmentPreference.screenName = screenName;
        return temperatureFragmentPreference;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_humanity_preference, container, false);
        TextView title = (TextView) rootView.findViewById(R.id.preference_title);
        title.setText(screenName);
        return rootView;
    }
}
