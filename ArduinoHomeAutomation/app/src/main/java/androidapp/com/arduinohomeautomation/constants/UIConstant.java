package androidapp.com.arduinohomeautomation.constants;

/**
 * Created by sandip.mahajan on 1/24/2015.
 *
 * This class will stored the UI constant values
 */
public interface UIConstant {

    /**
     * Timeout to show the splash screen to the user
     */
    int SPLASH_SCREEN_TIMEOUT = 3 * 1000;

}
