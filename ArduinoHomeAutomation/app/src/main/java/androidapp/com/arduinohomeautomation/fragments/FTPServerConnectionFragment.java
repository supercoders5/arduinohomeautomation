package androidapp.com.arduinohomeautomation.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;
import java.net.InetAddress;

import androidapp.com.arduinohomeautomation.R;
import androidapp.com.arduinohomeautomation.tasks.FTPLoginTask;
import androidapp.com.arduinohomeautomation.util.ValidationUtil;


public class FTPServerConnectionFragment extends Fragment {

    private EditText ftpIPAddress;
    private EditText loginName;
    private EditText password;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_ftp_server_connection, container, false);
        ftpIPAddress = (EditText) rootView.findViewById(R.id.ftp_ip_address_edit);
        loginName = (EditText) rootView.findViewById(R.id.login_edit);
        password = (EditText) rootView.findViewById(R.id.password_edit);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_save:
                String ftpIpAddressValue = ftpIPAddress.getText().toString();
                String loginNameValue = loginName.getText().toString();
                String passwordValue = password.getText().toString();

                if(ValidationUtil.isNotEmpty(ftpIpAddressValue, loginNameValue, passwordValue)) {
                    new FTPLoginTask(getActivity()).execute(ftpIpAddressValue, loginNameValue, passwordValue);
                } else {
                    Toast.makeText(getActivity(), "Fill all fields", Toast.LENGTH_SHORT).show();
                }

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
