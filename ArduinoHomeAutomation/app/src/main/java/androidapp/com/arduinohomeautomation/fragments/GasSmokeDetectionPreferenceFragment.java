package androidapp.com.arduinohomeautomation.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;

import androidapp.com.arduinohomeautomation.R;
import androidapp.com.arduinohomeautomation.util.SeekBarPreference;

/**
 * Created by sandip.mahajan on 1/26/2015.
 */
public class GasSmokeDetectionPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private String screenName;
    private SeekBarPreference desiredTempPref;

    public static GasSmokeDetectionPreferenceFragment instance(String screenName) {
        GasSmokeDetectionPreferenceFragment lightPreferenceFragment = new GasSmokeDetectionPreferenceFragment();
        lightPreferenceFragment.screenName = screenName;
        return lightPreferenceFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.gas_smoke_detection_preference);
        // Set listener :
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        int value = 0;
        desiredTempPref = (SeekBarPreference) findPreference("gas_smoke_detection_desired_temp_preference");
        value = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getInt("gas_smoke_detection_desired_temp_preference", 0);
        desiredTempPref.setSummary(getResources().getString(R.string.setting_summary) + " " + value + " " + getResources().getString(R.string.degree_celcius));
        Preference preference = findPreference("gas_smoke_detection_preference_title");
        preference.setTitle(screenName);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        int value = 0;
        if ("gas_smoke_detection_desired_temp_preference".equalsIgnoreCase(key)) {
            value = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getInt("gas_smoke_detection_desired_temp_preference", 0);
            desiredTempPref.setSummary(getResources().getString(R.string.setting_summary) + " " + value + " " + getResources().getString(R.string.degree_celcius));
        }
    }
}
