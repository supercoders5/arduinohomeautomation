package androidapp.com.arduinohomeautomation.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;

import androidapp.com.arduinohomeautomation.R;

/**
 * Created by sandip.mahajan on 1/26/2015.
 */
public class WaterDetectionPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private String screenName;

    public static WaterDetectionPreferenceFragment instance(String screenName) {
        WaterDetectionPreferenceFragment lightPreferenceFragment = new WaterDetectionPreferenceFragment();
        lightPreferenceFragment.screenName = screenName;
        return lightPreferenceFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.water_detection_preference);
        // Set listener :
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        Preference preference = findPreference("water_detection_preference_title");
        preference.setTitle(screenName);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }
}
