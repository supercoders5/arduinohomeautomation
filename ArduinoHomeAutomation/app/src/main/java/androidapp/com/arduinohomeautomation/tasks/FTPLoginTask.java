package androidapp.com.arduinohomeautomation.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by massy on 08.02.15.
 */
public class FTPLoginTask extends AsyncTask<String, String, Boolean> {

    Context mContext;
    ProgressDialog progressDialog;

    public FTPLoginTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        String address = params[0];
        String login = params[1];
        String password = params[2];

        String host = address;
        int port = 21;

        Pattern p = Pattern.compile("^\\s*(.*?):(\\d+)\\s*$");
        Matcher m = p.matcher(address);
        if (m.matches()) {
            host = m.group(1);
            port = Integer.parseInt(m.group(2));
        }

        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(host, port);
            ftpClient.login(login, password);
            
            /*
            Errors are caused by this code:
            if(ftpClient.getStatus() == null) throw new IllegalArgumentException("status cant be null");
            System.out.println("status :: " + ftpClient.getStatus());
            
            Output from FileZilla:
            (000032)17.02.2015 15:46:58 - user1 (192.168.1.5)> STAT
            (000032)17.02.2015 15:46:58 - user1 (192.168.1.5)> 500 Syntax error, command unrecognized.
            
            forum.filezilla-project.org Site-Admin: "The STAT command is not implemented by FileZilla Server, 
            an optional command according to the FTP specifications. Not supporting STAT is entirely harmless."
            */
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean isLogged) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if(isLogged)
            Toast.makeText(mContext, "Login Successful", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "Login Unsuccessful", Toast.LENGTH_SHORT).show();
    }
}
