package androidapp.com.arduinohomeautomation.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import androidapp.com.arduinohomeautomation.MainUIScreen;
import androidapp.com.arduinohomeautomation.R;
import androidapp.com.arduinohomeautomation.adapter.GridViewAdapter;
import androidapp.com.arduinohomeautomation.data.MenuItems;

/**
 * Created by sandip.mahajan on 1/25/2015.
 * <p/>
 * A placeholder fragment containing a simple view.
 */
public class CameraVideoFragment extends Fragment {

    private String screenName;

    public static CameraVideoFragment instance(String screenName) {
        CameraVideoFragment subMenuFragment = new CameraVideoFragment();
        subMenuFragment.screenName = screenName;
        return subMenuFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_submenu_grid_view, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.menuTitleText);
        textView.setText(screenName);
        GridView gridView = (GridView) rootView.findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Fragment fragment = null;
                switch (index) {
                    case MenuItems.RECORD_VIDEO_MENU:
                        fragment = RecordVideoFragment.instance(screenName);
                        break;
                    case MenuItems.SAVED_VIDEOS_MENU:
                        fragment = SavedPicturesVideosFragment.instance(screenName, MenuItems.SAVED_VIDEOS_MENU);
                        break;
                    case MenuItems.SAVED_PICTURES_MENU:
                        fragment = SavedPicturesVideosFragment.instance(screenName, MenuItems.SAVED_PICTURES_MENU);
                        break;
                }
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,
                        fragment).addToBackStack(null).commit();
                ((MainUIScreen) getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                ((MainUIScreen) getActivity()).mDrawerToggle.setDrawerIndicatorEnabled(false);
                ((MainUIScreen) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }
        });
        GridViewAdapter gridViewAdapter = new GridViewAdapter();
        gridViewAdapter.setData(MenuItems.cameraVideoMenu);
        gridView.setAdapter(gridViewAdapter);
        // Set the Required Animation to GridView and start the Animation
        Animation anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.fly_in_from_top_corner);
        gridView.setAnimation(anim);
        anim.start();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
