package androidapp.com.arduinohomeautomation;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;

import androidapp.com.arduinohomeautomation.fragments.MainMenuFragment;
import androidapp.com.arduinohomeautomation.fragments.SettingFragment;


public class MainUIScreen extends AbstractBaseActivity {

    private final static int HOME_MENU = 0;
    private final static int SETTING_MENU = 1;
    private final static int HELP_MENU = 2;

    public static final String ARG_SECTION_NUMBER = "section_number";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle = getTitle();
    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main_uiscreen;
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }

    @Override
    public void selectItem(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, position);
        Fragment fragment = null;
        if (position == HOME_MENU) {
            fragment = new MainMenuFragment();
            fragment.setArguments(args);
        } else if (position == SETTING_MENU) {
            fragment = new SettingFragment();
            fragment.setArguments(args);
        } else if (position == HELP_MENU) {
            return;
        }
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        mDrawerList.setSelection(position);
        setTitle(navMenuTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }
}
