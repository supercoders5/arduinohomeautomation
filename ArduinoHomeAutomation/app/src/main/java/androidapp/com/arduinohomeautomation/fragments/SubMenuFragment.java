package androidapp.com.arduinohomeautomation.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import androidapp.com.arduinohomeautomation.MainUIScreen;
import androidapp.com.arduinohomeautomation.R;
import androidapp.com.arduinohomeautomation.adapter.GridViewAdapter;
import androidapp.com.arduinohomeautomation.data.MenuItems;

/**
 * Created by sandip.mahajan on 1/25/2015.
 * <p/>
 * A placeholder fragment containing a simple view.
 */
public class SubMenuFragment extends Fragment {

    private String screenName;
    private int screenIndex;

    public static SubMenuFragment instance(String screenName, int screenIndex) {
        SubMenuFragment subMenuFragment = new SubMenuFragment();
        subMenuFragment.screenName = screenName;
        subMenuFragment.screenIndex = screenIndex;
        return subMenuFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_submenu_grid_view, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.menuTitleText);
        textView.setText(screenName);
        GridView gridView = (GridView) rootView.findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Fragment fragment = null;
                if (screenIndex == MenuItems.BATHROOM_MENU) {
                    if (index == 4 || index == 5)
                        index += 2;
                }
                switch (index) {
                    case MenuItems.LIGHT_PREFERENCE_MENU:
                        fragment = LightPreferenceFragment.instance(screenName);
                        break;
                    case MenuItems.TEMPERATURE_PREFERENCE_MENU:
                        fragment = TemperatureFragmentPreference.instance(screenName);
                        break;
                    case MenuItems.HUMIDITY_PREFERENCE_MENU:
                        fragment = HumanityFragmentPreference.instance(screenName);
                        break;
                    case MenuItems.MOTION_PREFERENCE_MENU:
                        fragment = MotionDetectionPreferenceFragment.instance(screenName);
                        break;
                    case MenuItems.SOUND_PREFERENCE_MENU:
                        fragment = SoundPreferenceFragment.instance(screenName);
                        break;
                    case MenuItems.CAMERA_PREFERENCE_MENU:
                        fragment = CameraVideoFragment.instance(screenName);
                        break;
                    case MenuItems.WATER_PREFERENCE_MENU:
                        fragment = WaterDetectionPreferenceFragment.instance(screenName);
                        break;
                    case MenuItems.GAS_SMOKE_PREFERENCE_MENU:
                        fragment = GasSmokeDetectionPreferenceFragment.instance(screenName);
                        break;
                }
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,
                        fragment).addToBackStack(null).commit();
                ((MainUIScreen) getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                ((MainUIScreen) getActivity()).mDrawerToggle.setDrawerIndicatorEnabled(false);
                ((MainUIScreen) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }
        });
        GridViewAdapter gridViewAdapter = new GridViewAdapter();
        gridViewAdapter.setData(MenuItems.getItems(screenIndex));
        gridView.setAdapter(gridViewAdapter);
        // Set the Required Animation to GridView and start the Animation
        Animation anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.fly_in_from_top_corner);
        gridView.setAnimation(anim);
        anim.start();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
