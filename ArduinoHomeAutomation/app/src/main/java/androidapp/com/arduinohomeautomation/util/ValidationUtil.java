package androidapp.com.arduinohomeautomation.util;

/**
 * Created by massy on 08.02.15.
 */
public class ValidationUtil {

    public static boolean isNotEmpty(String... values) {
        for(String s : values){
            if(s == null || s.isEmpty()) return false;
        }
        return true;
    }
}
