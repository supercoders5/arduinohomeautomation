package androidapp.com.arduinohomeautomation.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidapp.com.arduinohomeautomation.MainUIScreen;
import androidapp.com.arduinohomeautomation.R;


public class CustomRoomsListFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_custom_rooms_screen, container, false);
        ImageButton addButton = (ImageButton) rootView.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                createRoomAction();
            }
        });
        return rootView;
    }

    public void createRoomAction() {
        CustomRoomCreateFragment customRoomCreateFragment = new CustomRoomCreateFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,
                customRoomCreateFragment).addToBackStack(null).commit();
        ((MainUIScreen) getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainUIScreen) getActivity()).mDrawerToggle.setDrawerIndicatorEnabled(false);
        ((MainUIScreen) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }
}
