package androidapp.com.arduinohomeautomation.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidapp.com.arduinohomeautomation.R;

/**
 * Created by sandip.mahajan on 1/25/2015.
 */
public class GridViewAdapter extends BaseAdapter {

    private String[] data;

    public void setData(String[] data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int index) {
        return data[index];
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public View getView(int index, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.grid_item, viewGroup, false);
        }

        TextView text = (TextView) view.findViewById(R.id.text);
        text.setText(getItem(index).toString());
        view.setTag(getItem(index).toString());
        return view;
    }
}
