package androidapp.com.arduinohomeautomation.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.widget.TextView;

import androidapp.com.arduinohomeautomation.R;
import androidapp.com.arduinohomeautomation.util.SeekBarPreference;

/**
 * Created by sandip.mahajan on 1/26/2015.
 */
public class LightPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private SeekBarPreference brightnessPref;
    private SeekBarPreference lightONDesiredTempPref;
    private SeekBarPreference lightOFFDesiredTempPref;

    private String screenName;

    public static LightPreferenceFragment instance(String screenName) {
        LightPreferenceFragment lightPreferenceFragment = new LightPreferenceFragment();
        lightPreferenceFragment.screenName = screenName;
        return lightPreferenceFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.light_preferences);
        // Set listener :
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        int value = 0;

        brightnessPref = (SeekBarPreference) findPreference("light_brightness_preference");
        value = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getInt("light_brightness_preference", 0);
        brightnessPref.setSummary(getResources().getString(R.string.setting_summary) + " " + value + " " + getResources().getString(R.string.percent));

        lightONDesiredTempPref = (SeekBarPreference) findPreference("light_on_event_desired_temp_preference");
        value = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getInt("light_on_event_desired_temp_preference", 0);
        lightONDesiredTempPref.setSummary(getResources().getString(R.string.setting_summary) + " " + value + " " + getResources().getString(R.string.degree_celcius));

        lightOFFDesiredTempPref = (SeekBarPreference) findPreference("light_off_event_desired_temp_preference");
        value = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getInt("light_off_event_desired_temp_preference", 0);
        lightOFFDesiredTempPref.setSummary(getResources().getString(R.string.setting_summary) + " " + value + " " + getResources().getString(R.string.degree_celcius));

        Preference preference = findPreference("light_preference_title");
        preference.setTitle(screenName);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        // Set seekbar summary :
        int value = 0;
        if ("light_brightness_preference".equalsIgnoreCase(key)) {
            value = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getInt("light_brightness_preference", 0);
            brightnessPref.setSummary(getResources().getString(R.string.setting_summary) + " " + value + " " + getResources().getString(R.string.percent));
        } else if ("light_on_event_desired_temp_preference".equalsIgnoreCase(key)) {
            value = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getInt("light_on_event_desired_temp_preference", 0);
            lightONDesiredTempPref.setSummary(getResources().getString(R.string.setting_summary) + " " + value + " " + getResources().getString(R.string.degree_celcius));
        } else if ("light_off_event_desired_temp_preference".equalsIgnoreCase(key)) {
            value = PreferenceManager.getDefaultSharedPreferences(this.getActivity()).getInt("light_off_event_desired_temp_preference", 0);
            lightOFFDesiredTempPref.setSummary(getResources().getString(R.string.setting_summary) + " " + value + " " + getResources().getString(R.string.degree_celcius));
        }
    }
}
