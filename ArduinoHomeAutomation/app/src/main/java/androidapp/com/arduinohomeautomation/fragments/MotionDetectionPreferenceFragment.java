package androidapp.com.arduinohomeautomation.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;

import androidapp.com.arduinohomeautomation.R;
import androidapp.com.arduinohomeautomation.util.SeekBarPreference;

/**
 * Created by sandip.mahajan on 1/26/2015.
 */
public class MotionDetectionPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private String screenName;

    public static MotionDetectionPreferenceFragment instance(String screenName) {
        MotionDetectionPreferenceFragment lightPreferenceFragment = new MotionDetectionPreferenceFragment();
        lightPreferenceFragment.screenName = screenName;
        return lightPreferenceFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.motion_detection_preference);
        // Set listener :
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        Preference preference = findPreference("motion_detection_preference_title");
        preference.setTitle(screenName);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }
}
