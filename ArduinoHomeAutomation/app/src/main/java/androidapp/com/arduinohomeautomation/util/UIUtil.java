package androidapp.com.arduinohomeautomation.util;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

import androidapp.com.arduinohomeautomation.AbstractBaseActivity;

/**
 * Created by sandip.mahajan on 1/24/2015.
 * <p/>
 * This class will consist of all Utility methods
 */
public class UIUtil {

    /**
     * This will register handler for each activity
     *
     * @param context
     */
    public static Handler registerWeakHandler(AbstractBaseActivity context) {
        return new UIhandler(context);
    }

    /**
     * Navigation to specific UI screen
     *
     * @param context
     * @param tClass
     */
    public static void navigateUI(Context context, Class tClass) {
        Intent intent = new Intent(context, tClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * Weak reference handler to remove handler leaks
     * Created by sandip.mahajan on 1/24/2015.
     *
     * @param <AbstractBaseActivity>
     */
    private static class UIhandler extends Handler {
        private final WeakReference<AbstractBaseActivity> reference;

        public UIhandler(AbstractBaseActivity activity) {
            reference = new WeakReference<AbstractBaseActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            AbstractBaseActivity activity = reference.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }
}
