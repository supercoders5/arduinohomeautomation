package androidapp.com.arduinohomeautomation.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidapp.com.arduinohomeautomation.R;

/**
 * Created by sandip.mahajan on 1/26/2015.
 */
public class RecordVideoFragment extends PreferenceFragment {

    private String screenName;

    public static RecordVideoFragment instance(String screenName) {
        RecordVideoFragment temperatureFragmentPreference = new RecordVideoFragment();
        temperatureFragmentPreference.screenName = screenName;
        return temperatureFragmentPreference;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_record_video, container, false);
        TextView title = (TextView) rootView.findViewById(R.id.menuTitleText);
        title.setText(screenName);
        return rootView;
    }
}
