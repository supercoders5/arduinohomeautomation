package androidapp.com.arduinohomeautomation.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidapp.com.arduinohomeautomation.R;

/**
 * Created by sandip.mahajan on 1/26/2015.
 */
public class TemperatureFragmentPreference extends PreferenceFragment {

    private String screenName;

    public static TemperatureFragmentPreference instance(String screenName) {
        TemperatureFragmentPreference temperatureFragmentPreference = new TemperatureFragmentPreference();
        temperatureFragmentPreference.screenName = screenName;
        return temperatureFragmentPreference;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_temperature_preference, container, false);
        TextView title = (TextView) rootView.findViewById(R.id.preference_title);
        title.setText(screenName);
        return rootView;
    }
}
