package androidapp.com.arduinohomeautomation.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;

import androidapp.com.arduinohomeautomation.MainUIScreen;
import androidapp.com.arduinohomeautomation.R;
import androidapp.com.arduinohomeautomation.adapter.GridViewAdapter;
import androidapp.com.arduinohomeautomation.data.MenuItems;

/**
 * Created by sandip.mahajan on 1/25/2015.
 */
public class SettingFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_grid_view, container, false);
        GridView gridView = (GridView) rootView.findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Fragment fragment = null;
                if (index == 0) {
                    fragment = new FTPServerConnectionFragment();
                } else {
                    fragment = new FTPServerFileSettingsFragment();
                }
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,
                        fragment).addToBackStack(null).commit();
                ((MainUIScreen) getActivity()).mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                ((MainUIScreen) getActivity()).mDrawerToggle.setDrawerIndicatorEnabled(false);
                ((MainUIScreen) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }
        });
        GridViewAdapter gridViewAdapter = new GridViewAdapter();
        gridViewAdapter.setData(MenuItems.settingsMenuItems);
        gridView.setAdapter(gridViewAdapter);
        // Set the Required Animation to GridView and start the Animation
        // use fly_in_from_center to have 2nd type of animation effect (snapshot 2)
        Animation anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.fly_in_from_top_corner);
        gridView.setAnimation(anim);
        anim.start();

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
