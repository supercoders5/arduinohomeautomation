package androidapp.com.arduinohomeautomation.util;

import android.content.Context;
import android.graphics.Color;
import android.preference.PreferenceCategory;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

/**
 * Created by sandip.mahajan on 1/26/2015.
 */
public class SPreferenceCategory extends PreferenceCategory {
    public SPreferenceCategory(Context context) {
        super(context);
    }

    public SPreferenceCategory(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SPreferenceCategory(Context context, AttributeSet attrs,
                                int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        TextView titleView = (TextView) view.findViewById(android.R.id.title);
        titleView.setTextSize(16);
        titleView.setTextColor(Color.RED);
    }
}
